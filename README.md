# Kvm install role
## Actions
* install (default) - install kvm on ubuntu
* clone_nombr - clone vm nombr
* resize_nombr - resize disk vm nombr
* start - start vm
* stop - stop vm

## Install
### Install python (ubuntu)
For use ansible, you need install python2.7 on remote server.
```bash
ansible kvm -m raw -a "apt-get -qq update;\
apt-get -y --no-install-recommends install python2.7 python-apt"
```

### ansible.cfg example
```bash
cat <<'EOF' > ansible.cfg
[defaults]
hostfile = hosts
remote_user = root
deprecation_warnings = False
roles_path = roles
force_color = 1
retry_files_enabled = False
executable = /bin/bash
allow_world_readable_tmpfiles=True

[ssh_connection]
ssh_args = -o ControlMaster=auto -o ControlPersist=60s -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no
pipelining = True
EOF
```

### download kvm role
```bash
mkdir ./roles
cat <<'EOF' >> ./roles/req.yml

- src: git+https://gitlab.com/kapuza-ansible/kvm.git
  name: kvm
EOF
echo "roles/kvm" >> .gitignore
ansible-galaxy install --force -r ./roles/req.yml
```

## Kvm (ubuntu)
Install and setup kvm server.
```bash
cat << 'EOF' > kvm.yml
---
- hosts:
    kvm
  tasks:
    - name: KVM install
      include_role:
        name: kvm
      vars:
        kvm_action: install
        kvm_remove_default_net: true

    - name: KVM clone_nombr
      include_role:
        name: kvm
      vars:
        kvm_action: 'clone_nombr'
        kvm_name: 'ubuntu-test'
        kvm_domain: 'home.xmu'
        kvm_memory: 2097152
        kvm_vcpu: 2
        kvm_disk_create: true
        kvm_disk_size: '5GB'
        kvm_disk_name: '{{ kvm_name }}'
        kvm_disk_vg: 'vg'
        kvm_disk_oldname: 'ubuntu1604-nombr'
        kvm_disk_oldvg: 'vg'
        kvm_os_kernel: '/var/lib/libvirt/kernels/vmlinuz-4.4.0-127-generic'
        kvm_os_initrd: '/var/lib/libvirt/kernels/initrd.img-4.4.0-127-generic'
        kvm_os_cmdline: 'root=/dev/vda'
        kvm_interface_type: 'bridge'
        kvm_interface_source: 'br0'
        #kvm_interface_mac: if not set - auto
        # use Macvtap
        #kvm_interface_type: 'direct'
        #kvm_interface_source: 'enp3s0'
        #kvm_interface_mode: 'bridge'
        kvm_interface_model_type: 'virtio'
        kvm_net_source: 'ens3'
        kvm_net_static: false
        kvm_net_addr: '192.168.1.152'
        kvm_net_mask: '192.168.1.1'
        kvm_net_gateway: '255.255.255.0'
        kvm_net_nameservers: '192.168.1.1'

    - name: KVM resize_nombr
      include_role:
        name: kvm
      vars:
        kvm_action: 'resize_nombr'
        kvm_name: 'ubuntu-test'
        kvm_disk_name: '{{ kvm_name }}'
        kvm_disk_vg: 'vg'
        kvm_disk_size: '5GB'
        kvm_resize_stop_before: true
        kvm_resize_start_after: true

    - name: KVM start VM
      include_role:
        name: kvm
      vars:
        kvm_action: start
        kvm_name: 'ubuntu-test'

    - name: KVM stop VM
      include_role:
        name: kvm
      vars:
        kvm_action: stop
        kvm_name: 'ubuntu-test'
EOF

ansible-playbook ./kvm.yml
```
